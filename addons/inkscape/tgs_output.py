import sys
import argparse
from distutils.util import strtobool
 


if __name__ == "__main__":
    if sys.version_info.major < 3:
        import subprocess
        sys.exit(subprocess.call(["python3"] + sys.argv))

    parser = argparse.ArgumentParser()
    parser.add_argument("infile")
    parser.add_argument(
        "--fps",
        type=int,
        help="Frames per second",
        default=60
    )
    parser.add_argument(
        "--frames",
        type=int,
        help="Number of frames",
        default=60
    )
    parser.add_argument(
        "--format",
        choices=["tgs", "lottie"],
        help="Output format",
        default="tgs"
    )
    parser.add_argument(
        "--pretty",
        type=strtobool,
        help="Pretty print JSON",
        default=1
    )
    parser.add_argument(
        "--sanitize",
        type=strtobool,
        help="Sanitize",
        default=0
    )
    parser.add_argument(
        "--validate",
        type=strtobool,
        help="Validate",
        default=0
    )
    parser.add_argument(
        "--smartanimate",
        type=strtobool,
        help="Smart animate",
        default=0
    )
    parser.add_argument(
        "--lottiepath",
        help="Additional path to add to sys.path",
        default=""
    )
    parser.add_argument(
        "--layer-frames",
        type=int,
        help="If greater than 0, treats every layer in the SVG as a different animation frame, "
            "greater values increase the time each frames lasts for.",
        default=0
    )
    ns, _ = parser.parse_known_args()
    if ns.lottiepath:
        sys.path.insert(0, ns.lottiepath)
    import lottie

    if ns.smartanimate:

        from xml.etree import ElementTree
        from lottie import objects

        class CustomSVGParser(lottie.parsers.svg.importer.SvgParser):
          def parse_etree(self, etree, layer_frames=0, *args, **kwargs):
             animation = objects.Animation(*args, **kwargs)
             self.animation = animation
             self.max_time = 0
             self.document = etree

             svg = etree.getroot()

             self._get_dpi(svg)

             if "width" in svg.attrib and "height" in svg.attrib:
                  animation.width = int(round(self._parse_unit(svg.attrib["width"])))
                  animation.height = int(round(self._parse_unit(svg.attrib["height"])))
             else:
                  _, _, animation.width, animation.height = map(int, svg.attrib["viewBox"].split(" "))
             animation.name = self._get_name(svg, self.qualified("sodipodi", "docname"))
   
             if layer_frames:
                  permanent_layers=[]
                  for frame in reversed(svg):
                      if self.unqualified(frame.tag) == "g":
                          group_name=self._get_name(frame, self.qualified("inkscape", "label"))
                          if "[" in group_name and "]" in group_name[group_name.index("["):]:
                             grouptag=group_name[group_name.index("[")+1:group_name.index("]")]
                             for sequence in grouptag.split(","):
                                 if ":" in sequence:
                                    begin,end=sequence.split(":")
                                 else:
                                    begin=end=sequence
                                 begin=int(begin)
                                 end=int(end)+1
                                 layer = objects.ShapeLayer()
                                 layer.in_point = begin * layer_frames
                                 layer.out_point = end * layer_frames
                                 animation.add_layer(layer)
                                 self._parseshape_g(frame, layer, {})
                                 self.max_time = max(self.max_time,layer.out_point)
                          else:
                                 layer = objects.ShapeLayer()
                                 layer.in_point = 0
                                 animation.add_layer(layer)
                                 self._parseshape_g(frame, layer, {})
                                 permanent_layers.append(layer)
                  for layer in permanent_layers:
                         layer.out_point = self.max_time
             else:
                  self._svg_to_layer(animation, svg)
   
             if self.max_time:
                  animation.out_point = self.max_time
   
             self._fix_viewbox(svg, (layer for layer in animation.layers if not layer.parent_index))

             return animation

        # from lottie.parsers.svg.importer: 1294
        def parse_svg_etree(etree, layer_frames=0, *args, **kwargs):
            parser = CustomSVGParser()
            return parser.parse_etree(etree, layer_frames, *args, **kwargs)
   
        def parse_svg_file(file, layer_frames=0, *args, **kwargs):
            return parse_svg_etree(ElementTree.parse(file), layer_frames, *args, **kwargs)
        animation = parse_svg_file(ns.infile, ns.layer_frames, ns.frames, ns.fps)
    else:
        animation = lottie.parsers.svg.importer.parse_svg_file(ns.infile, ns.layer_frames, ns.frames, ns.fps)
    if ns.format == "lottie":
        lottie.exporters.export_lottie(animation, sys.stdout, ns.pretty)
    else:
        if ns.validate:
          import tempfile
          with tempfile.NamedTemporaryFile() as tmpfile:
           lottie.exporters.export_tgs(animation, tmpfile.name, sanitize=ns.sanitize, validate=ns.validate)
           with open(tmpfile.name,"rb") as tmp:
            sys.stdout.buffer.write(tmp.read())
        else:
          lottie.exporters.export_tgs(animation, sys.stdout.buffer, sanitize=ns.sanitize, validate=ns.validate)
